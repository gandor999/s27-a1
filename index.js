let http = require("http");

http.createServer(function(request, response) {
	// Home
	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}

	// Profile
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile');
	}

	// Recources
	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available");
	}

	// Add
	if(request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add course to our resources');
	}

	// Update
	if(request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources');
	}

	// Archive
	if(request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive courses to our recources');
	}
}).listen(4000);

console.log('Server is running at localhost:4000')